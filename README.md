# OAuth2 Authentication with Panopticon Visualization Server

Following steps are required to run OAuth2 based authentication with Panopticon Visualization Server.
 
### 1. Setup OAUTH2 Server:

1. Build and Run Oauth2 Server docker image

	`> .\bin\start.bat`

	The above execution will stop at command where it needs to create a user.

	`docker-compose exec server python manage.py createsuperuser`

	To create new user, enter username and password. Example,
	
		>...
		Username (leave blank to use 'root'): testuser
	    Email address: (leave blank)
		Password: password123
		Password (again): password123
		Superuser created successfully.

	To recreate the docker image, container, and superuser, you can run prune command,

	`> .\bin\prune.bat`

2. Verify if Server is running

	Go to [http://localhost:9977/admin](http://localhost:9977/admin)

	Login with credentials that you created in *Step 1*. 

	Successful login will take you to the `Site Administratio GUI`.


3. Add __Application__ in `Site Administratio GUI`

	- Click the link `Applications` under `Django OAuth Toolkit`

	Fill in the Add application form:

	1. Enter `1` as _id_ of User since we only have one user.
	2. Enter `http://localhost:8080/panopticon/server/rest/auth/login` as _Redirect uris_. Use the authentication URl for the Visualization Server. It will be,
	3. Select `Confidential` as _Client type_
	4. Select `Authorization code` as _Authorization grant type_
	5. Enter `Panopticon VizServer` as _Name_ of the application

		__IMPORTANT:__ Before _Saving_ the application, please note down the 
			- Client Id &
			- Client secret
		These credentials are necessary in the authorization flow.
	6. Press __SAVE__ to finish the add application process

### 2. Configure Panopticon Visualization Server

1. Update `Panopticon.properties` file, located in C:\vizserverdata

	Update the following properties accordingly,

		authentication.oauth2.client.id=rxPRrI2XXjilXpHQzlnGLAdnitCjnUU7Qdt6kDQ2
		authentication.oauth2.client.secret=9PZwGIsvoWP2zyPdTBr9nxCyuLHvb2imfpaQuf0SmGwZvUC41mOyYmipRfOAsLZHABSiTWC4ONecHIsUzZFcMJ1LHyD0QnXd4yMzeaFPcqvRznWmKQrf5SIIc50GrmWV
		authentication.oauth2.identity.attribute.username=testuser
		authentication.oauth2.identity.url=http://localhost:9977/profile/
		authentication.oauth2.login.callback.url=http://localhost:8080/panopticon/server/rest/auth/login
		authentication.oauth2.login.response.type=code
		authentication.oauth2.login.scope=
		authentication.oauth2.login.state=
		authentication.oauth2.login.url=http://localhost:9977/o/authorize/
		authentication.oauth2.logout.url=
		authentication.oauth2.redirect=
		authentication.oauth2.token.method=query
		authentication.oauth2.token.url=http://localhost:9977/o/token/
		authentication.type=OAUTH2
	
2. Run _Visualization Server_ from _Tomcat_ setup. Assumption is that you have Visualization Server already setup.

	`> catalina.sh jpda run`

3. Login with AuthorizationCode flow

	1. Press Login. Enter credentials you have created in OAuth2 setup _(testuser/password123)_
	2. It will show a window asking for Permission to Authorize _Panopticon VizServer_ to grant Read/Write permissions. Press __Authorize__

	On Successful authentication, you shall go to the default /workbooks/ page. The page will show the logged in user's name like `Welcome, testuser`.
