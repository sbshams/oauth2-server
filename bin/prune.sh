docker ps -q | xargs -r docker stop -f
docker ps -a -q | xargs -r docker rm -f
docker images -q  --no-trunc | xargs -r docker rmi
docker volume ls -q | xargs -r docker volume rm
